Class {
	#name : #MidnightBlueTheme,
	#superclass : #PharoDarkTheme,
	#category : #MidnightBlueTheme
}

{ #category : #accessing }
MidnightBlueTheme class >> baseColor [
	^ Color fromHexString: '0d3642'
]

{ #category : #accessing }
MidnightBlueTheme class >> basePassiveBackgroundColor [
	^ self baseColor
]

{ #category : #accessing }
MidnightBlueTheme class >> darkBaseColor [
	^ Color fromHexString: '062a36'
]

{ #category : #accessing }
MidnightBlueTheme class >> lightBaseColor [
	^ Color fromHexString: '124f61'
]

{ #category : #accessing }
MidnightBlueTheme class >> themeName [
	^ 'Midnight Blue'
]

{ #category : #'accessing colors' }
MidnightBlueTheme >> backgroundColor [
	^ self class darkBaseColor 
]

{ #category : #'accessing colors' }
MidnightBlueTheme >> desktopColor [
	^ self darkBaseColor darker
]

{ #category : #'accessing colors' }
MidnightBlueTheme >> lightBackgroundColor [ 
	^ self class lightBaseColor
]

{ #category : #'accessing colors' }
MidnightBlueTheme >> shStyleTable [

	^ SHRBTextStyler midnightBlueStyleTable
]

{ #category : #'accessing colors' }
MidnightBlueTheme >> textColor [ 
	^ Color fromHexString: 'ede8d5'
]

{ #category : #'accessing colors' }
MidnightBlueTheme >> textEditorNormalFillStyleFor: aTextEditor [
	
	^ self textColor

]

{ #category : #'accessing colors' }
MidnightBlueTheme >> textFieldNormalFillStyleFor: aTextField [
	
	^ self textColor

]

{ #category : #'accessing colors' }
MidnightBlueTheme >> unrelatedContextStyleFor: aContext [

	^ {TextColor color: (self textColor)}
]

{ #category : #'accessing colors' }
MidnightBlueTheme >> worldMainDockingBarNormalFillStyleFor: aDockingBar [
	"Return the world main docking bar fillStyle for the given docking bar."
	
	|aColor|
	aColor := aDockingBar originalColor alpha: 0.7.
	^(GradientFillStyle ramp: {
			0.0->(aColor alphaMixed: 0.3 with: (self textColor alpha: aColor alpha)).
			0.8->aColor darker.
			1.0->aColor darker duller})
		origin: aDockingBar topLeft;
		direction: (aDockingBar isVertical
			ifTrue: [aDockingBar width @ 0]
			ifFalse: [0 @ aDockingBar height]);
		radial: false
]
